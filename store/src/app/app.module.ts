import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { CartModule } from './features/moduls/cart/cart.module';
import { ProductsModule } from './features/moduls/products/products.module';
import { ButtonComponent } from './shared/components/button/button.component';
import { InputComponent } from './shared/components/input/input.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, CoreModule, ProductsModule, CartModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
